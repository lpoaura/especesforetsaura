# ForetAuraVue

Site internet de présentation des espèces de vertébrés forestiers à enjeux en Auvergne Rhône-Alpes développé par la LPO Auvergne-Rhône-Alpes dans le cadre d'un programme mené avec le soutien financier de la région Auvergne-Rhône-Alpes/

Site développé avec VueJS, Leaflet (vue2-leaflet) et BootStrap (bootstrap-vue.js).

# Addresse de l'appli

https://aura-partage.lpo.fr/cartes/foretaura

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
